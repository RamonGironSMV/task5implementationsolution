﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenCvSharp;
using InterfaceImageOperators;

namespace ClassTask5RamonGiron
{
    public class ImageOperatorsclass : IImageOperators
    {

        /// <summary>
        /// Returns an image cleared to specific value.
        /// [2.1/2.2]
        /// </summary>
        public Mat ClearImage(Mat img, float intensity)
        {
            //Declare the return Mat and the indexers for the Mats.
            Mat clearedImage = clearedImage = new Mat(img.Height, img.Width, img.Type());
            var mat3 = new Mat<Vec3b>(img);
            var indexer = mat3.GetIndexer();
            var mat4 = new Mat<Vec3b>(clearedImage);
            var indexer2 = mat4.GetIndexer();

            //The path on Mat object  to change  the pixel value. 
            for (int y = 0; y < img.Height; y++)
            {
                for (int x = 0; x < img.Width; x++)
                {
                    Vec3b color = indexer[y, x];
                    color.Item0 = Convert.ToByte(intensity);
                    color.Item1 = Convert.ToByte(intensity);
                    color.Item2 = Convert.ToByte(intensity);
                    indexer2[y, x] = color;
                }

            }
            return clearedImage;
        }

        /// <summary>
        /// Returns an image cleared to specific value.
        /// [2.1/2.2]
        /// </summary>
        public Mat ClearImage(Mat img)
        {
            //Declare the return Mat and the indexers for the Mats.
            Mat clearedImage = new Mat(img.Height, img.Width, img.Type());
            int intensity = 0;
            var mat3 = new Mat<Vec3b>(img);
            var indexer = mat3.GetIndexer();
            var mat4 = new Mat<Vec3b>(clearedImage);
            var indexer2 = mat4.GetIndexer();

            //The path on Mat object  to change  the pixel value.
            for (int y = 0; y < img.Height; y++)
            {
                for (int x = 0; x < img.Width; x++)
                {
                    Vec3b color = indexer[y, x];
                    color.Item0 = Convert.ToByte(intensity);
                    color.Item1 = Convert.ToByte(intensity);
                    color.Item2 = Convert.ToByte(intensity);
                    indexer2[y, x] = color;
                }

            }
            return clearedImage;
        }

        /// <summary>
        /// Returns a copied image  and image pixelwise.
        /// [2.3]
        /// </summary>
        public Mat CopyImage(Mat img)
        {
            //Declare the return Mat and the indexers for the Mats.
            Mat copyImage = new Mat(img.Height, img.Width, img.Type());

            var mat1 = new Mat<Vec3b>(img);
            var indexer = mat1.GetIndexer();
            var mat2 = new Mat<Vec3b>(copyImage);
            var indexer2 = mat2.GetIndexer();

            //The path on Mat object  to copy the pixel value. 
            for (int y = 0; y < img.Height; y++)
            {
                for (int x = 0; x < img.Width; x++)
                {
                    Vec3b color = indexer[y, x];
                    indexer2[y, x] = color;
                }

            }
            return copyImage;
        }

        /// <summary>
        /// Returns a inverted image.
        /// [2.4]
        /// </summary>
        public Mat InvertImage(Mat img)
        {
            //Declare the return Mat and the indexers for the Mats.
            Mat invertedImage = new Mat(img.Height, img.Width, img.Type());
            var mat1 = new Mat<Vec3b>(img);
            var indexer = mat1.GetIndexer();
            var mat2 = new Mat<Vec3b>(invertedImage);
            var indexer2 = mat2.GetIndexer();

            //The path on Mat object  to invert the pixel value. 
            for (int y = 0; y < img.Height; y++)
            {
                for (int x = 0; x < img.Width; x++)
                {
                    Vec3b color = indexer[y, x];
                    color.Item0 = Convert.ToByte(255 - color.Item0);
                    color.Item1 = Convert.ToByte(255 - color.Item1);
                    color.Item2 = Convert.ToByte(255 - color.Item2);
                    indexer2[y, x] = color;
                }

            }
            return invertedImage;
        }

        /// <summary>
        /// Returns a shifted image arbitrary many steps in any of the 8 directions.
        /// [2.5/2.6]
        /// </summary>
        public Mat ShiftImage(Mat img, DirectionsEnum directionsEnum, float steps, float optSteps)
        {
            //Declare the return Mat and the indexers for the Mats.
            Mat shiftImage = new Mat(img.Height, img.Width, img.Type());
            var mat1 = new Mat<Vec3b>(img);
            var indexer = mat1.GetIndexer();
            var mat2 = new Mat<Vec3b>(shiftImage);
            var indexer2 = mat2.GetIndexer();

            //The path on Mat object to set zeros.
            for (int y = 0; y < img.Height; y++)
            {
                for (int x = 0; x < img.Width; x++)
                {
                    Vec3b color = indexer[y, x];
                    Vec3b color2 = color;
                    color2.Item0 = 0;
                    color2.Item1 = 0;
                    color2.Item2 = 0;
                    indexer2[y, x] = color2;
                }
            }

            //Translate the img to the Top
            if (directionsEnum == DirectionsEnum.Top)
            {
                for (int y = Convert.ToInt32(steps); y < img.Height - steps; y++)
                {
                    for (int x = 0; x < img.Width; x++)
                    {
                        Vec3b color = indexer[y, x];
                        indexer2[y - Convert.ToInt32(steps), x] = color;
                    }
                }
            }

            //Translate the img to the top right
            if (directionsEnum == DirectionsEnum.TopRight)
            {
                for (int y = Convert.ToInt32(optSteps); y < img.Height; y++)
                {
                    for (int x = 0; x < img.Width - steps; x++)
                    {
                        Vec3b color = indexer[y, x];
                        indexer2[y - Convert.ToInt32(optSteps), x + Convert.ToInt32(steps)] = color;
                    }
                }
            }

            //Translate the img to the top left
            if (directionsEnum == DirectionsEnum.TopLeft)
            {
                for (int y = Convert.ToInt32(optSteps); y < img.Height; y++)
                {
                    for (int x = Convert.ToInt32(steps); x < img.Width; x++)
                    {
                        Vec3b color = indexer[y, x];
                        indexer2[y - Convert.ToInt32(optSteps), x - Convert.ToInt32(steps)] = color;
                    }
                }
            }
            //Translate the img to the right
            if (directionsEnum == DirectionsEnum.Right)
            {
                for (int y = 0; y < img.Height; y++)
                {
                    for (int x = 0; x < img.Width - steps; x++)
                    {
                        Vec3b color = indexer[y, x];
                        indexer2[y, x + Convert.ToInt32(steps)] = color;
                    }
                }
            }

            //Translate the img to the left
            if (directionsEnum == DirectionsEnum.Left)
            {
                for (int y = 0; y < img.Height; y++)
                {
                    for (int x = Convert.ToInt32(steps); x < img.Width; x++)
                    {
                        Vec3b color = indexer[y, x];
                        indexer2[y, x - Convert.ToInt32(steps)] = color;
                    }
                }
            }

            //Translate the img to the bottom
            if (directionsEnum == DirectionsEnum.Bottom)
            {
                for (int y = 0; y < img.Height - Convert.ToInt32(steps); y++)
                {
                    for (int x = 0; x < img.Width; x++)
                    {
                        Vec3b color = indexer[y, x];
                        indexer2[y + Convert.ToInt32(steps), x] = color;
                    }
                }
            }
            //Translate the img to the bottom right
            if (directionsEnum == DirectionsEnum.BottomRight)
            {
                for (int y = 0; y < img.Height - optSteps; y++)
                {
                    for (int x = 0; x < img.Width - steps; x++)
                    {
                        Vec3b color = indexer[y, x];
                        indexer2[y + Convert.ToInt32(optSteps), x + Convert.ToInt32(steps)] = color;
                    }
                }
            }

            //Translate the img to the  bottom left
            if (directionsEnum == DirectionsEnum.BottomLeft)
            {
                for (int y = 0; y < img.Height - optSteps; y++)
                {
                    for (int x = Convert.ToInt32(steps); x < img.Width; x++)
                    {
                        Vec3b color = indexer[y, x];
                        indexer2[y + Convert.ToInt32(optSteps), x - Convert.ToInt32(steps)] = color;
                    }
                }
            }
            return shiftImage;
        }

        /// <summary>
        /// Returns a shifted image arbitrary many steps in any of the 8 directions.
        /// [2.5/2.6]
        /// </summary>
        public Mat ShiftImage(Mat img, Mat translationMatrix)
        {
            //Declare the return Mat and the indexers for the Mats.
            Mat shiftImage = new Mat(img.Height, img.Width, img.Type());
            var mat1 = new Mat<Vec3b>(img);
            var indexer = mat1.GetIndexer();
            var mat2 = new Mat<Vec3b>(shiftImage);
            var indexer2 = mat2.GetIndexer();
            var mat3 = new Mat<float>(translationMatrix);
            var indexer3 = mat3.GetIndexer();

            //The path on Mat object to set zeros.
            for (int y = 0; y < img.Height; y++)
            {
                for (int x = 0; x < img.Width; x++)
                {
                    Vec3b color = indexer[y, x];
                    Vec3b color2 = color;
                    color2.Item0 = 0;
                    color2.Item1 = 0;
                    color2.Item2 = 0;
                    indexer2[y, x] = color2;
                }
            }

            //Translate the img to the Top
            if (indexer3[0, 2] == 0 && indexer3[1, 2] < 0)
            {
                for (int y = Convert.ToInt32(indexer3[1, 2] * (-1)); y < img.Height; y++)
                {
                    for (int x = 0; x < img.Width; x++)
                    {
                        Vec3b color = indexer[y, x];
                        indexer2[y - Convert.ToInt32(indexer3[1, 2] * (-1)), x] = color;
                    }
                }
            }

            //Translate the img to the Top Right
            if (indexer3[0, 2] > 0 && indexer3[1, 2] < 0)
            {

                for (int y = Convert.ToInt32(indexer3[1, 2] * (-1)); y < img.Height; y++)
                {
                    for (int x = 0; x < img.Width - indexer3[0, 2]; x++)
                    {
                        Vec3b color = indexer[y, x];
                        indexer2[y - Convert.ToInt32(indexer3[1, 2] * (-1)), x + Convert.ToInt32(indexer3[0, 2])] = color;
                    }
                }
            }
            //Translate the img to the Top Left
            if (indexer3[0, 2] < 0 & indexer3[1, 2] < 0)
            {
                for (int y = Convert.ToInt32(indexer3[1, 2] * (-1)); y < img.Height; y++)
                {
                    for (int x = Convert.ToInt32(indexer3[0, 2] * (-1)); x < img.Width; x++)
                    {
                        Vec3b color = indexer[y, x];
                        indexer2[y - Convert.ToInt32(indexer3[1, 2] * (-1)), x - Convert.ToInt32(indexer3[0, 2] * (-1))] = color;
                    }
                }
            }

            //Translate the img to the right
            if (indexer3[0, 2] > 0 & indexer3[1, 2] == 0)
            {
                for (int y = 0; y < img.Height; y++)
                {
                    for (int x = 0; x < img.Width - indexer3[0, 2]; x++)
                    {
                        Vec3b color = indexer[y, x];
                        indexer2[y, x + Convert.ToInt32(indexer3[0, 2])] = color;
                    }
                }
            }

            //Translate the img to the left 
            if (indexer3[0, 2] < 0 & indexer3[1, 2] == 0)
            {
                for (int y = 0; y < img.Height; y++)
                {
                    for (int x = Convert.ToInt32(indexer3[0, 2] * (-1)); x < img.Width; x++)
                    {
                        Vec3b color = indexer[y, x];
                        indexer2[y, x - Convert.ToInt32(indexer3[0, 2] * (-1))] = color;
                    }
                }
            }

            //Translate the img to the bottom
            if (indexer3[0, 2] == 0 & indexer3[1, 2] > 0)
            {
                for (int y = 0; y < img.Height - Convert.ToInt32(indexer3[1, 2]); y++)
                {
                    for (int x = 0; x < img.Width; x++)
                    {
                        Vec3b color = indexer[y, x];
                        indexer2[y + Convert.ToInt32(indexer3[1, 2]), x] = color;
                    }
                }
            }
            //Translate the img to the bottom right
            if (indexer3[0, 2] >= 0 & indexer3[1, 2] >= 0)
            {
                for (int y = 0; y < img.Height - indexer3[1, 2]; y++)
                {
                    for (int x = 0; x < img.Width - indexer3[0, 2]; x++)
                    {
                        Vec3b color = indexer[y, x];
                        indexer2[y + Convert.ToInt32(indexer3[1, 2]), x + Convert.ToInt32(indexer3[0, 2])] = color;
                    }
                }
            }

            //Translate the img to the bottom left
            if (indexer3[0, 2] < 0 & indexer3[1, 2] > 0)
            {
                for (int y = 0; y < img.Height - indexer3[1, 2]; y++)
                {
                    for (int x = Convert.ToInt32(indexer3[0, 2] * (-1)); x < img.Width; x++)
                    {
                        Vec3b color = indexer[y, x];
                        indexer2[y + Convert.ToInt32(indexer3[1, 2]), x - Convert.ToInt32(indexer3[0, 2] * (-1))] = color;
                    }
                }
            }
            return shiftImage;
        }

        /// <summary>
        /// Returns a shifted image arbitrary many steps in any of the 8 directions.
        /// [2.5/2.6]
        /// </summary>
        public Mat ShiftImage(Mat img, float xSteps, float ySteps)
        {
            //Declare the return Mat and the indexers for the Mats.
            Mat shiftImage = new Mat(img.Height, img.Width, img.Type());
            var mat1 = new Mat<Vec3b>(img);
            var indexer = mat1.GetIndexer();
            var mat2 = new Mat<Vec3b>(shiftImage);
            var indexer2 = mat2.GetIndexer();
            //The path on Mat object to set zeros.
            for (int y = 0; y < img.Height; y++)
            {
                for (int x = 0; x < img.Width; x++)
                {
                    Vec3b color = indexer[y, x];
                    Vec3b color2 = color;
                    color2.Item0 = 0;
                    color2.Item1 = 0;
                    color2.Item2 = 0;
                    indexer2[y, x] = color2;
                }
            }

            //Translate the img to the Top
            if (xSteps == 0 & ySteps < 0)
            {
                for (int y = Convert.ToInt32(ySteps * (-1)); y < img.Height; y++)
                {
                    for (int x = 0; x < img.Width; x++)
                    {
                        Vec3b color = indexer[y, x];
                        indexer2[y - Convert.ToInt32(ySteps * (-1)), x] = color;
                    }
                }
            }

            //Translate the img to the Top Right
            if (xSteps > 0 & ySteps < 0)
            {
                for (int y = Convert.ToInt32(ySteps * (-1)); y < img.Height; y++)
                {
                    for (int x = 0; x < img.Width - xSteps; x++)
                    {
                        Vec3b color = indexer[y, x];
                        indexer2[y - Convert.ToInt32(ySteps * (-1)), x + Convert.ToInt32(xSteps)] = color;
                    }
                }
            }
            //Translate the img to the Top Left
            if (xSteps < 0 & ySteps < 0)
            {
                for (int y = Convert.ToInt32(ySteps * (-1)); y < img.Height; y++)
                {
                    for (int x = Convert.ToInt32(xSteps * (-1)); x < img.Width; x++)
                    {
                        Vec3b color = indexer[y, x];
                        indexer2[y - Convert.ToInt32(ySteps * (-1)), x - Convert.ToInt32(xSteps * (-1))] = color;
                    }
                }
            }

            //Translate the img to the right
            if (xSteps > 0 & ySteps == 0)
            {
                for (int y = 0; y < img.Height; y++)
                {
                    for (int x = 0; x < img.Width - xSteps; x++)
                    {
                        Vec3b color = indexer[y, x];
                        indexer2[y, x + Convert.ToInt32(xSteps)] = color;
                    }
                }
            }

            //Translate the img to the left 
            if (xSteps < 0 & ySteps == 0)
            {
                for (int y = 0; y < img.Height; y++)
                {
                    for (int x = Convert.ToInt32(xSteps * (-1)); x < img.Width; x++)
                    {
                        Vec3b color = indexer[y, x];
                        indexer2[y, x - Convert.ToInt32(xSteps * (-1))] = color;
                    }
                }
            }

            //Translate the img to the bottom
            if (xSteps == 0 & ySteps > 0)
            {
                for (int y = 0; y < img.Height - Convert.ToInt32(ySteps); y++)
                {
                    for (int x = 0; x < img.Width; x++)
                    {
                        Vec3b color = indexer[y, x];
                        indexer2[y + Convert.ToInt32(ySteps), x] = color;
                    }
                }
            }
            //Translate the img to the bottom right
            if (xSteps >= 0 & ySteps >= 0)
            {
                for (int y = 0; y < img.Height - ySteps; y++)
                {
                    for (int x = 0; x < img.Width - xSteps; x++)
                    {
                        Vec3b color = indexer[y, x];
                        indexer2[y + Convert.ToInt32(ySteps), x + Convert.ToInt32(xSteps)] = color;
                    }
                }
            }

            //Translate the img to the bottom left
            if (xSteps < 0 & ySteps > 0)
            {
                for (int y = 0; y < img.Height - ySteps; y++)
                {
                    for (int x = Convert.ToInt32(xSteps * (-1)); x < img.Width; x++)
                    {
                        Vec3b color = indexer[y, x];
                        indexer2[y + Convert.ToInt32(ySteps), x - Convert.ToInt32(xSteps * (-1))] = color;
                    }
                }
            }
            return shiftImage;
        }

        /// <summary>
        /// Returns an additioned of constant to an image. 
        /// [2.7]
        /// </summary>
        public Mat AddConstant(Mat img, float constantvalue)
        {
            //Declare the return Mat and the indexers for the Mats.
            Mat addedConstat = new Mat(img.Height, img.Width, img.Type());
            var mat1 = new Mat<Vec3b>(img);
            var indexer1 = mat1.GetIndexer();
            var mat4 = new Mat<Vec3b>(addedConstat);
            var indexer2 = mat4.GetIndexer();

            //The path on Mat object to add a constant.
            for (int y = 0; y < img.Height; y++)
            {
                for (int x = 0; x < img.Width; x++)
                {
                    Vec3b info = indexer1[y, x];
                    if ((info.Item0 + constantvalue) < 255)
                    {
                        info.Item0 = Convert.ToByte(info.Item0 + constantvalue);
                    }
                    else
                    {
                        info.Item0 = Convert.ToByte(255);
                    }
                    if ((info.Item1 + constantvalue) < 255)
                    {
                        info.Item1 = Convert.ToByte(info.Item1 + constantvalue);
                    }
                    else
                    {
                        info.Item1 = Convert.ToByte(255);
                    }
                    if ((info.Item2 + constantvalue) < 255)
                    {
                        info.Item2 = Convert.ToByte(info.Item2 + constantvalue);
                    }
                    else
                    {
                        info.Item2 = Convert.ToByte(255);
                    }
                    indexer2[y, x] = info;
                }
            }
            return addedConstat;
        }

        /// <summary>
        /// Returns a stretched contrast image. 
        /// [2.8/2.9]
        /// </summary>
        public Mat Scaling(Mat img, float gamma, float beta)
        {
            //Declare the return Mat and the indexers for the Mats.
            Mat scaledImage = new Mat(img.Height, img.Width, img.Type());
            var mat1 = new Mat<Vec3b>(img);
            var indexer = mat1.GetIndexer();
            var mat2 = new Mat<Vec3b>(scaledImage);
            var indexer2 = mat2.GetIndexer();
            //The path on Mat object to stretch contrast image 
            for (int y = 0; y < img.Height; y++)
            {
                for (int x = 0; x < img.Width; x++)
                {
                    Vec3b info = indexer[y, x];
                    if ((info.Item0 * gamma + beta) < 0)
                    {
                        info.Item0 = Convert.ToByte(0);
                    }
                    else if ((info.Item0 * gamma + beta) > 255)
                    {
                        info.Item0 = Convert.ToByte(255);
                    }
                    else
                    {
                        info.Item0 = Convert.ToByte(info.Item0 * gamma + beta);
                    }

                    if ((info.Item1 * gamma + beta) < 0)
                    {
                        info.Item1 = Convert.ToByte(0);
                    }
                    else if ((info.Item1 * gamma + beta) > 255)
                    {
                        info.Item1 = Convert.ToByte(255);
                    }
                    else
                    {
                        info.Item1 = Convert.ToByte(info.Item1 * gamma + beta);
                    }

                    if ((info.Item2 * gamma + beta) < 0)
                    {
                        info.Item2 = Convert.ToByte(0);
                    }
                    else if ((info.Item2 * gamma + beta) > 255)
                    {
                        info.Item2 = Convert.ToByte(255);
                    }
                    else
                    {
                        info.Item2 = Convert.ToByte(info.Item2 * gamma + beta);
                    }
                    indexer2[y, x] = info;
                }
            }
            return scaledImage;
        }

        /// <summary>
        /// Returns a Threshold image. 
        /// [2.10]
        /// </summary>
        public Mat ThresholdImage(Mat img, float thresholdValue)
        {
            //Declare the return Mat and the indexers for the Mats.
            Mat thresholdedImage = new Mat(img.Height, img.Width, img.Type());
            var mat1 = new Mat<Vec3b>(img);
            var indexer = mat1.GetIndexer();
            var mat2 = new Mat<Vec3b>(thresholdedImage);
            var indexer2 = mat2.GetIndexer();
            //The path on Mat object to threshold 
            for (int y = 0; y < img.Height; y++)
            {
                for (int x = 0; x < img.Width; x++)
                {
                    Vec3b color = indexer[y, x];
                    byte pixelValue = color.Item0;
                    byte pixelValue1 = color.Item1;
                    byte pixelValue2 = color.Item2;
                    if (pixelValue > thresholdValue)
                    {
                        pixelValue = 255;
                    }
                    else
                    {
                        pixelValue = 0;
                    }
                    if (pixelValue1 > thresholdValue)
                    {
                        pixelValue1 = 255;
                    }
                    else
                    {
                        pixelValue1 = 0;
                    }
                    if (pixelValue2 > thresholdValue)
                    {
                        pixelValue2 = 255;
                    }
                    else
                    {
                        pixelValue2 = 0;
                    }
                    color.Item0 = pixelValue;
                    color.Item1 = pixelValue1;
                    color.Item2 = pixelValue2;
                    indexer2[y, x] = color;
                }
            }
            return thresholdedImage;
        }

        /// <summary>
        /// Returns a Threshold image. 
        /// [2.10]
        /// </summary>
        public Mat ThresholdImage(Mat img, int lowerThreshold, int upperThreshold)
        {
            //Declare the return Mat and the indexers for the Mats.
            Mat thresholdedImage = new Mat(img.Height, img.Width, img.Type());
            var mat1 = new Mat<Vec3b>(img);
            var indexer = mat1.GetIndexer();
            var mat2 = new Mat<Vec3b>(thresholdedImage);
            var indexer2 = mat2.GetIndexer();
            //The path on Mat object to threshold 
            for (int y = 0; y < img.Height; y++)
            {
                for (int x = 0; x < img.Width; x++)
                {
                    Vec3b color = indexer[y, x];
                    byte pixelValue = color.Item0;
                    byte pixelValue1 = color.Item1;
                    byte pixelValue2 = color.Item2;
                    if ((lowerThreshold < pixelValue) && (pixelValue < upperThreshold))
                    {
                        pixelValue = 255;
                    }
                    else
                    {
                        pixelValue = 0;
                    }
                    if ((lowerThreshold < pixelValue1) && (pixelValue1 < upperThreshold))
                    {
                        pixelValue1 = 255;
                    }
                    else
                    {
                        pixelValue1 = 0;
                    }
                    if ((lowerThreshold < pixelValue2) && (pixelValue2 < upperThreshold))
                    {
                        pixelValue2 = 255;
                    }
                    else
                    {
                        pixelValue2 = 0;
                    }
                    color.Item0 = pixelValue;
                    color.Item1 = pixelValue1;
                    color.Item2 = pixelValue2;
                    indexer2[y, x] = color;
                }


            }
            return thresholdedImage;

        }
        /// <summary>
        /// Returns a Threshold image. 
        /// [2.10] [2.12] 
        /// </summary>
        public Mat ThresholdImage(Mat img, int lowerThreshold, int upperThreshold, bool invertedThresh)
        {
            //Declare the return Mat and the indexers for the Mats.
            Mat thresholdedImage = new Mat(img.Height, img.Width, img.Type());
            var mat1 = new Mat<Vec3b>(img);
            var indexer = mat1.GetIndexer();
            var mat2 = new Mat<Vec3b>(thresholdedImage);
            var indexer2 = mat2.GetIndexer();
            //The path on Mat object to threshold 
            for (int y = 0; y < img.Height; y++)
            {
                for (int x = 0; x < img.Width; x++)
                {
                    Vec3b color = indexer[y, x];
                    byte pixelValue = color.Item0;
                    byte pixelValue1 = color.Item1;
                    byte pixelValue2 = color.Item2;
                    if ((lowerThreshold < pixelValue) && (pixelValue < upperThreshold))
                    {
                        pixelValue = 255;
                    }
                    else
                    {
                        pixelValue = 0;
                    }
                    if ((lowerThreshold < pixelValue1) && (pixelValue1 < upperThreshold))
                    {
                        pixelValue1 = 255;
                    }
                    else
                    {
                        pixelValue1 = 0;
                    }
                    if ((lowerThreshold < pixelValue2) && (pixelValue2 < upperThreshold))
                    {
                        pixelValue2 = 255;
                    }
                    else
                    {
                        pixelValue2 = 0;
                    }
                    color.Item0 = pixelValue;
                    color.Item1 = pixelValue1;
                    color.Item2 = pixelValue2;
                    indexer2[y, x] = color;
                }


            }
            if (invertedThresh == true)
            { thresholdedImage = InvertImage(thresholdedImage); }

            return thresholdedImage;
        }

        /// <summary>
        /// Returns a Threshold image. 
        /// [2.10] [2.12] 
        /// </summary>
        public Mat ThresholdImage(Mat img, int lowerThreshold, int upperThreshold, bool invertedThresh, float keepVal)
        {
            //Declare the return Mat and the indexers for the Mats.
            Mat thresholdedImage = new Mat(img.Height, img.Width, img.Type());
            var mat1 = new Mat<Vec3b>(img);
            var indexer = mat1.GetIndexer();
            var mat2 = new Mat<Vec3b>(thresholdedImage);
            var indexer2 = mat2.GetIndexer();
            //The path on Mat object to threshold 
            for (int y = 0; y < img.Height; y++)
            {
                for (int x = 0; x < img.Width; x++)
                {
                    Vec3b color = indexer[y, x];
                    byte pixelValue = color.Item0;
                    byte pixelValue1 = color.Item1;
                    byte pixelValue2 = color.Item2;
                    if ((lowerThreshold < pixelValue) && (pixelValue < upperThreshold))
                    {
                        if (invertedThresh == true)
                        {
                            pixelValue = 0;
                        }
                        else
                        {
                            if (keepVal < 0)
                            { pixelValue = color.Item0; }
                            else
                            { pixelValue = Convert.ToByte(keepVal); }

                        }
                    }
                    else
                    {
                        if (invertedThresh == true)
                        {
                            if (keepVal < 0)
                            { pixelValue = color.Item0; }
                            else
                            { pixelValue = Convert.ToByte(keepVal); }

                        }
                        else
                        { pixelValue = 0; }
                    }
                    if ((lowerThreshold < pixelValue1) && (pixelValue1 < upperThreshold))
                    {

                        if (invertedThresh == true)
                        {
                            pixelValue1 = 0;
                        }
                        else
                        {
                            if (keepVal < 0)
                            { pixelValue1 = color.Item1; }
                            else
                            { pixelValue1 = Convert.ToByte(keepVal); }

                        }
                    }
                    else
                    {
                        if (invertedThresh == true)
                        {
                            if (keepVal < 0)
                            { pixelValue1 = color.Item1; }
                            else
                            { pixelValue1 = Convert.ToByte(keepVal); }

                        }
                        else
                        { pixelValue1 = 0; }
                    }
                    if ((lowerThreshold < pixelValue2) && (pixelValue2 < upperThreshold))
                    {

                        if (invertedThresh == true)
                        {
                            pixelValue2 = 0;
                        }
                        else
                        {
                            if (keepVal < 0)
                            { pixelValue2 = color.Item2; }
                            else
                            { pixelValue2 = Convert.ToByte(keepVal); }

                        }
                    }
                    else
                    {
                        if (invertedThresh == true)
                        {
                            if (keepVal < 0)
                            { pixelValue2 = color.Item2; }
                            else
                            { pixelValue2 = Convert.ToByte(keepVal); }

                        }
                        else
                        { pixelValue2 = 0; }
                    }
                    color.Item0 = pixelValue;
                    color.Item1 = pixelValue1;
                    color.Item2 = pixelValue2;
                    indexer2[y, x] = color;
                }
            }

            if (invertedThresh == true)
            { thresholdedImage = InvertImage(thresholdedImage); }
            return thresholdedImage;

        }

        /// <summary>
        /// Returns a stretched  the contrast of the input binary image for visualisation.
        /// [2.13] 
        /// </summary>
        public Mat VisualiseBinary(Mat img)
        {
            //Declare the return Mat and the indexers for the Mats.
            Mat visualizedImage = new Mat(img.Height, img.Width, MatType.CV_8U);
            var mat1 = new Mat<Vec3b>(img);
            var indexer = mat1.GetIndexer();
            var mat2 = new Mat<Vec3b>(visualizedImage);
            var indexer2 = mat2.GetIndexer();

            //The path on Mat object to visualise Binary
            for (int y = 0; y < img.Height; y++)
            {
                for (int x = 0; x < img.Width; x++)
                {
                    Vec3b color = indexer[y, x];
                    color.Item0 = Convert.ToByte(255 - Convert.ToInt32(color.Item0));
                    indexer2[y, x] = color;
                }
            }
            return visualizedImage;
        }

        /// <summary>
        /// Returns a eroded image.
        /// [2.14/2.15]
        /// </summary>
        public Mat Erosion(Mat img)
        { //Declare the return Mat and the indexers for the Mats.
            Mat erodedImage = new Mat(img.Height, img.Width, img.Type());
            var mat1 = new Mat<Vec3b>(img);
            var indexer = mat1.GetIndexer();
            var mat2 = new Mat<Vec3b>(erodedImage);
            var indexer2 = mat2.GetIndexer();
            int sigma = 0, sigma1 = 0, sigma2 = 0; ;
            int xStart = 0, yStart = 0, xFinish = 0, yFinish = 0;
            int matrixSize = 0;
            int maxPixelValue = 0, maxPixelValue1 = 0, maxPixelValue2 = 0;
            int minPixelValue = 0, minPixelValue1 = 0, minPixelValue2 = 0;

            //The path on Mat object to erotion
            for (int y = 0; y < img.Height; y++)
            {
                for (int x = 0; x < img.Width; x++)
                {

                    Vec3b color = indexer[y, x];
                    Vec3b color2 = color;
                    minPixelValue = color.Item0;
                    minPixelValue1 = color.Item1;
                    minPixelValue2 = color.Item2;
                    maxPixelValue = color.Item0;
                    maxPixelValue1 = color.Item1;
                    maxPixelValue2 = color.Item2;
                    //Limits
                    //Center
                    if (x != 0 && x != img.Width - 1 && y != 0 && y != img.Height - 1)
                    {
                        yStart = y - 1; yFinish = y + 1;
                        xStart = x - 1; xFinish = x + 1;
                        matrixSize = 9;
                    }
                    //Upper limit
                    if (y == 0 && x != 0 && x != img.Width - 1)
                    {
                        yStart = y; yFinish = y + 1;
                        xStart = x - 1; xFinish = x + 1;
                        matrixSize = 6;
                    }
                    //Bottom limit
                    else if (y == img.Height - 1 && x != 0 && x != img.Width - 1)
                    {
                        yStart = y - 1; yFinish = y;
                        xStart = x - 1; xFinish = x + 1;
                        matrixSize = 6;
                    }
                    //Right limit
                    else if (x == img.Width - 1 && y != 0 && y != img.Height - 1)
                    {

                        yStart = y - 1; yFinish = y + 1;
                        xStart = x - 1; xFinish = x;
                        matrixSize = 6;
                    }
                    //Left limit
                    else if (x == 0 && y != 0 && y != img.Height - 1)
                    {
                        yStart = y - 1; yFinish = y + 1;
                        xStart = x; xFinish = x + 1;
                        matrixSize = 6;
                    }
                    else
                    {
                        //Corner Upper left
                        if (y == 0 && x == 0)
                        {
                            yStart = y; yFinish = y + 1;
                            xStart = x; xFinish = x + 1;
                            matrixSize = 4;
                        }
                        //Corner Upper Right
                        if (y == 0 && x == img.Width - 1)
                        {
                            yStart = y; yFinish = y + 1;
                            xStart = x - 1; xFinish = x;
                            matrixSize = 4; ;
                        }
                        //Corner Botton left
                        if (y == img.Height - 1 && x == img.Width - 1)
                        {
                            yStart = y - 1; yFinish = y;
                            xStart = x - 1; xFinish = x;
                            matrixSize = 4;
                        }
                        //Corner botton Right
                        if (y == img.Height - 1 && x == 0)
                        {
                            yStart = y - 1; yFinish = y;
                            xStart = x; xFinish = x + 1;
                            matrixSize = 4;
                        }
                    }
                    for (int jj = yStart; jj <= yFinish; jj++)
                    {
                        for (int ii = xStart; ii <= xFinish; ii++)
                        {
                            Vec3b info = indexer[jj, ii];
                            sigma = info.Item0 + sigma;
                            sigma1 = info.Item1 + sigma1;
                            sigma2 = info.Item2 + sigma2;

                            if (maxPixelValue < info.Item0)
                            { maxPixelValue = info.Item0; }
                            if (maxPixelValue1 < info.Item1)
                            { maxPixelValue1 = info.Item1; }
                            if (maxPixelValue2 < info.Item2)
                            { maxPixelValue2 = info.Item2; }
                            if (minPixelValue > info.Item0)
                            { minPixelValue = info.Item0; }
                            if (minPixelValue1 > info.Item1)
                            { minPixelValue1 = info.Item1; }
                            if (minPixelValue2 > info.Item2)
                            { minPixelValue2 = info.Item2; }
                        }
                    }
                    sigma = sigma - color.Item0;
                    sigma1 = sigma1 - color.Item1;
                    sigma2 = sigma2 - color.Item2;

                    if (sigma < ((matrixSize - 1) * maxPixelValue))
                    {
                        color2.Item0 = Convert.ToByte(minPixelValue);
                        sigma = 0;
                    }
                    else
                    {
                        sigma = 0;
                    }
                    if (sigma1 < ((matrixSize - 1) * maxPixelValue1))
                    {
                        color2.Item1 = Convert.ToByte(minPixelValue1);
                        sigma1 = 0;
                    }
                    else
                    {
                        sigma1 = 0;
                    }
                    if (sigma2 < ((matrixSize - 1) * maxPixelValue2))
                    {
                        color2.Item2 = Convert.ToByte(minPixelValue2);
                        sigma2 = 0;
                    }
                    else
                    {
                        sigma2 = 0;
                    }

                    indexer2[y, x] = color2;
                }
            }
            return erodedImage;
        }

        /// <summary>
        /// Returns a dilated image.
        /// [2.14/2.15]
        /// </summary>
        public Mat Dilation(Mat img)
        {
            //Declare the return Mat and the indexers for the Mats.
            Mat dilatedImage = new Mat(img.Height, img.Width, img.Type());
            var mat1 = new Mat<Vec3b>(img);
            var indexer = mat1.GetIndexer();
            var mat2 = new Mat<Vec3b>(dilatedImage);
            var indexer2 = mat2.GetIndexer();
            int sigma = 0, sigma1 = 0, sigma2 = 0; ;
            int xStart = 0, yStart = 0, xFinish = 0, yFinish = 0;
            int maxPixelValue = 0, maxPixelValue1 = 0, maxPixelValue2 = 0;
            int minPixelValue = 0, minPixelValue1 = 0, minPixelValue2 = 0;

            //The path on Mat object to dilation
            for (int y = 0; y < img.Height; y++)
            {
                for (int x = 0; x < img.Width; x++)
                {

                    Vec3b color = indexer[y, x];
                    Vec3b color2 = color;
                    minPixelValue = color.Item0;
                    minPixelValue1 = color.Item1;
                    minPixelValue2 = color.Item2;
                    maxPixelValue = color.Item0;
                    maxPixelValue1 = color.Item1;
                    maxPixelValue2 = color.Item2;
                    //Limits
                    //Center
                    if (x != 0 && x != img.Width - 1 && y != 0 && y != img.Height - 1)
                    {
                        yStart = y - 1; yFinish = y + 1;
                        xStart = x - 1; xFinish = x + 1;
                    }
                    //Upper limit
                    if (y == 0 && x != 0 && x != img.Width - 1)
                    {
                        yStart = y; yFinish = y + 1;
                        xStart = x - 1; xFinish = x + 1;
                    }
                    //Bottom limit
                    else if (y == img.Height - 1 && x != 0 && x != img.Width - 1)
                    {
                        yStart = y - 1; yFinish = y;
                        xStart = x - 1; xFinish = x + 1;
                    }
                    //Right limit
                    else if (x == img.Width - 1 && y != 0 && y != img.Height - 1)
                    {

                        yStart = y - 1; yFinish = y + 1;
                        xStart = x - 1; xFinish = x;
                    }
                    //Left limit
                    else if (x == 0 && y != 0 && y != img.Height - 1)
                    {
                        yStart = y - 1; yFinish = y + 1;
                        xStart = x; xFinish = x + 1;
                    }
                    else
                    {
                        //Corner Upper left
                        if (y == 0 && x == 0)
                        {
                            yStart = y; yFinish = y + 1;
                            xStart = x; xFinish = x + 1;
                        }
                        //Corner Upper Right
                        if (y == 0 && x == img.Width - 1)
                        {
                            yStart = y; yFinish = y + 1;
                            xStart = x - 1; xFinish = x;
                        }
                        //Corner Botton left
                        if (y == img.Height - 1 && x == img.Width - 1)
                        {
                            yStart = y - 1; yFinish = y;
                            xStart = x - 1; xFinish = x;
                        }
                        //Corner botton Right
                        if (y == img.Height - 1 && x == 0)
                        {
                            yStart = y - 1; yFinish = y;
                            xStart = x; xFinish = x + 1;
                        }
                    }
                    for (int jj = yStart; jj <= yFinish; jj++)
                    {
                        for (int ii = xStart; ii <= xFinish; ii++)
                        {
                            Vec3b info = indexer[jj, ii];
                            sigma = info.Item0 + sigma;
                            sigma1 = info.Item1 + sigma1;
                            sigma2 = info.Item2 + sigma2;

                            if (maxPixelValue < info.Item0)
                            { maxPixelValue = info.Item0; }
                            if (maxPixelValue1 < info.Item1)
                            { maxPixelValue1 = info.Item1; }
                            if (maxPixelValue2 < info.Item2)
                            { maxPixelValue2 = info.Item2; }
                            if (minPixelValue > info.Item0)
                            { minPixelValue = info.Item0; }
                            if (minPixelValue1 > info.Item1)
                            { minPixelValue1 = info.Item1; }
                            if (minPixelValue2 > info.Item2)
                            { minPixelValue2 = info.Item2; }
                        }
                    }
                    sigma = sigma - color.Item0;
                    sigma1 = sigma1 - color.Item1;
                    sigma2 = sigma2 - color.Item2;

                    if (sigma > minPixelValue)
                    {
                        color2.Item0 = Convert.ToByte(maxPixelValue);
                        sigma = 0;
                    }
                    else
                    {
                        sigma = 0;
                    }
                    if (sigma1 > minPixelValue1)
                    {
                        color2.Item1 = Convert.ToByte(maxPixelValue1);
                        sigma1 = 0;
                    }
                    else
                    {
                        sigma1 = 0;
                    }
                    if (sigma2 > minPixelValue2)
                    {
                        color2.Item2 = Convert.ToByte(maxPixelValue2);
                        sigma2 = 0;
                    }
                    else
                    {
                        sigma2 = 0;
                    }

                    indexer2[y, x] = color2;
                }
            }
            return dilatedImage;
        }


        /// <summary>
        /// Returns a detected edges in a binary image 
        /// [2.17]
        /// </summary>
        public Mat BinaryBoundary(Mat img)
        {
            //Declare the return Mat and the indexers for the Mats.
            Mat edgeDetected = new Mat(img.Height, img.Width, MatType.CV_8U);
            var mat1 = new Mat<Vec3b>(img);
            var indexer = mat1.GetIndexer();
            var mat2 = new Mat<Vec3b>(edgeDetected);
            var indexer2 = mat2.GetIndexer();
            int sigma = 0;
            int xStart = 0, yStart = 0, xFinish = 0, yFinish = 0;

            int matrixSize = 0;

            //The path on Mat object to detect edges in a binary image
            for (int y = 0; y < img.Height; y++)
            {
                for (int x = 0; x < img.Width; x++)
                {

                    Vec3b color = indexer[y, x];
                    Vec3b color2 = color;

                    //Center
                    if (x != 0 && x != img.Width - 1 && y != 0 && y != img.Height - 1)
                    {
                        yStart = y - 1; yFinish = y + 1;
                        xStart = x - 1; xFinish = x + 1;
                        matrixSize = 9;
                    }
                    //Upper limit
                    if (y == 0 && x != 0 && x != img.Width - 1)
                    {
                        yStart = y; yFinish = y + 1;
                        xStart = x - 1; xFinish = x + 1;
                        matrixSize = 6;
                    }
                    //Bottom limit
                    else if (y == img.Height - 1 && x != 0 && x != img.Width - 1)
                    {
                        yStart = y - 1; yFinish = y;
                        xStart = x - 1; xFinish = x + 1;
                        matrixSize = 6;
                    }
                    //Right limit
                    else if (x == img.Width - 1 && y != 0 && y != img.Height - 1)
                    {

                        yStart = y - 1; yFinish = y + 1;
                        xStart = x - 1; xFinish = x;
                        matrixSize = 6;
                    }
                    //Left limit
                    else if (x == 0 && y != 0 && y != img.Height - 1)
                    {
                        yStart = y - 1; yFinish = y + 1;
                        xStart = x; xFinish = x + 1;
                        matrixSize = 6;
                    }
                    else
                    {
                        //Corner Upper left
                        if (y == 0 && x == 0)
                        {
                            yStart = y; yFinish = y + 1;
                            xStart = x; xFinish = x + 1;
                            matrixSize = 4;
                        }
                        //Corner Upper Right
                        if (y == 0 && x == img.Width - 1)
                        {
                            yStart = y; yFinish = y + 1;
                            xStart = x - 1; xFinish = x;
                            matrixSize = 4; ;
                        }
                        //Corner Botton left
                        if (y == img.Height - 1 && x == img.Width - 1)
                        {
                            yStart = y - 1; yFinish = y;
                            xStart = x - 1; xFinish = x;
                            matrixSize = 4;
                        }
                        //Corner botton Right
                        if (y == img.Height - 1 && x == 0)
                        {
                            yStart = y - 1; yFinish = y;
                            xStart = x; xFinish = x + 1;
                            matrixSize = 4;
                        }
                    }
                    for (int jj = yStart; jj <= yFinish; jj++)
                    {
                        for (int ii = xStart; ii <= xFinish; ii++)
                        {
                            Vec3b info = indexer[jj, ii];
                            sigma = info.Item0 + sigma;
                        }
                    }
                    sigma = sigma - color.Item0;
                    if (sigma == ((matrixSize - 1) * 255))
                    {
                        color2.Item0 = Convert.ToByte(0);
                        indexer2[y, x] = color2;
                        sigma = 0;
                    }
                    else
                    {
                        indexer2[y, x] = color;
                        sigma = 0;
                    }
                }
            }
            return edgeDetected;
        }


        /// <summary>
        /// Returns a removed salt noise image.
        /// [2.18]
        /// </summary>
        public Mat RemoveSaltNoise(Mat img)
        {   //Declare the return Mat and the indexers for the Mats.
            Mat saltRemovedImage = new Mat(img.Height, img.Width, img.Type());
            var mat1 = new Mat<Vec3b>(img);
            var indexer = mat1.GetIndexer();
            var mat2 = new Mat<Vec3b>(saltRemovedImage);
            var indexer2 = mat2.GetIndexer();
            int sigma = 0, sigma1 = 0, sigma2 = 0; ;
            int xStart = 0, yStart = 0, xFinish = 0, yFinish = 0;
            int matrixSize = 0;
            int maxPixelValue = 0, maxPixelValue1 = 0, maxPixelValue2 = 0;
            int minPixelValue = 0, minPixelValue1 = 0, minPixelValue2 = 0;

            //The path on Mat object to remove salt
            for (int y = 0; y < img.Height; y++)
            {
                for (int x = 0; x < img.Width; x++)
                {

                    Vec3b color = indexer[y, x];
                    Vec3b color2 = color;
                    minPixelValue = color.Item0;
                    minPixelValue1 = color.Item1;
                    minPixelValue2 = color.Item2;
                    maxPixelValue = color.Item0;
                    maxPixelValue1 = color.Item1;
                    maxPixelValue2 = color.Item2;
                    //Limits
                    //Center
                    if (x != 0 && x != img.Width - 1 && y != 0 && y != img.Height - 1)
                    {
                        yStart = y - 1; yFinish = y + 1;
                        xStart = x - 1; xFinish = x + 1;
                        matrixSize = 9;
                    }
                    //Upper limit
                    if (y == 0 && x != 0 && x != img.Width - 1)
                    {
                        yStart = y; yFinish = y + 1;
                        xStart = x - 1; xFinish = x + 1;
                        matrixSize = 6;
                    }
                    //Bottom limit
                    else if (y == img.Height - 1 && x != 0 && x != img.Width - 1)
                    {
                        yStart = y - 1; yFinish = y;
                        xStart = x - 1; xFinish = x + 1;
                        matrixSize = 6;
                    }
                    //Right limit
                    else if (x == img.Width - 1 && y != 0 && y != img.Height - 1)
                    {

                        yStart = y - 1; yFinish = y + 1;
                        xStart = x - 1; xFinish = x;
                        matrixSize = 6;
                    }
                    //Left limit
                    else if (x == 0 && y != 0 && y != img.Height - 1)
                    {
                        yStart = y - 1; yFinish = y + 1;
                        xStart = x; xFinish = x + 1;
                        matrixSize = 6;
                    }
                    else
                    {
                        //Corner Upper left
                        if (y == 0 && x == 0)
                        {
                            yStart = y; yFinish = y + 1;
                            xStart = x; xFinish = x + 1;
                            matrixSize = 4;
                        }
                        //Corner Upper Right
                        if (y == 0 && x == img.Width - 1)
                        {
                            yStart = y; yFinish = y + 1;
                            xStart = x - 1; xFinish = x;
                            matrixSize = 4; ;
                        }
                        //Corner Botton left
                        if (y == img.Height - 1 && x == img.Width - 1)
                        {
                            yStart = y - 1; yFinish = y;
                            xStart = x - 1; xFinish = x;
                            matrixSize = 4;
                        }
                        //Corner botton Right
                        if (y == img.Height - 1 && x == 0)
                        {
                            yStart = y - 1; yFinish = y;
                            xStart = x; xFinish = x + 1;
                            matrixSize = 4;
                        }
                    }
                    for (int jj = yStart; jj <= yFinish; jj++)
                    {
                        for (int ii = xStart; ii <= xFinish; ii++)
                        {
                            Vec3b info = indexer[jj, ii];
                            sigma = info.Item0 + sigma;
                            sigma1 = info.Item1 + sigma1;
                            sigma2 = info.Item2 + sigma2;

                            if (maxPixelValue < info.Item0)
                            { maxPixelValue = info.Item0; }
                            if (maxPixelValue1 < info.Item1)
                            { maxPixelValue1 = info.Item1; }
                            if (maxPixelValue2 < info.Item2)
                            { maxPixelValue2 = info.Item2; }
                            if (minPixelValue > info.Item0)
                            { minPixelValue = info.Item0; }
                            if (minPixelValue1 > info.Item1)
                            { minPixelValue1 = info.Item1; }
                            if (minPixelValue2 > info.Item2)
                            { minPixelValue2 = info.Item2; }
                        }
                    }
                    sigma = sigma - color.Item0;
                    sigma1 = sigma1 - color.Item1;
                    sigma2 = sigma2 - color.Item2;

                    if (sigma == ((matrixSize - 1) * maxPixelValue))
                    {
                        color2.Item0 = Convert.ToByte(maxPixelValue);
                        sigma = 0;
                    }
                    else
                    {
                        sigma = 0;
                    }
                    if (sigma1 == ((matrixSize - 1) * maxPixelValue1))
                    {
                        color2.Item1 = Convert.ToByte(maxPixelValue1);
                        sigma1 = 0;
                    }
                    else
                    {
                        sigma1 = 0;
                    }
                    if (sigma2 == ((matrixSize - 1) * maxPixelValue2))
                    {
                        color2.Item2 = Convert.ToByte(maxPixelValue2);
                        sigma2 = 0;
                    }
                    else
                    {
                        sigma2 = 0;
                    }

                    indexer2[y, x] = color2;
                }
            }
            return saltRemovedImage;
        }

        /// <summary>
        /// Returns a removed pepper noise image.
        /// [2.19]
        /// </summary>
        public Mat RemovePepperNoise(Mat img)
        {
            //Declare the return Mat and the indexers for the Mats.
            Mat pepperRemovedImage = new Mat(img.Height, img.Width, img.Type());
            var mat1 = new Mat<Vec3b>(img);
            var indexer = mat1.GetIndexer();
            var mat2 = new Mat<Vec3b>(pepperRemovedImage);
            var indexer2 = mat2.GetIndexer();
            int sigma = 0, sigma1 = 0, sigma2 = 0; ;
            int xStart = 0, yStart = 0, xFinish = 0, yFinish = 0;
            int maxPixelValue = 0, maxPixelValue1 = 0, maxPixelValue2 = 0;
            int minPixelValue = 0, minPixelValue1 = 0, minPixelValue2 = 0;

            //The path on Mat object to remove pepper
            for (int y = 0; y < img.Height; y++)
            {
                for (int x = 0; x < img.Width; x++)
                {

                    Vec3b color = indexer[y, x];
                    Vec3b color2 = color;
                    minPixelValue = color.Item0;
                    minPixelValue1 = color.Item1;
                    minPixelValue2 = color.Item2;
                    maxPixelValue = color.Item0;
                    maxPixelValue1 = color.Item1;
                    maxPixelValue2 = color.Item2;
                    //Limits
                    //Center
                    if (x != 0 && x != img.Width - 1 && y != 0 && y != img.Height - 1)
                    {
                        yStart = y - 1; yFinish = y + 1;
                        xStart = x - 1; xFinish = x + 1;
                      
                    }
                    //Upper limit
                    if (y == 0 && x != 0 && x != img.Width - 1)
                    {
                        yStart = y; yFinish = y + 1;
                        xStart = x - 1; xFinish = x + 1;
                     
                    }
                    //Bottom limit
                    else if (y == img.Height - 1 && x != 0 && x != img.Width - 1)
                    {
                        yStart = y - 1; yFinish = y;
                        xStart = x - 1; xFinish = x + 1;
                  
                    }
                    //Right limit
                    else if (x == img.Width - 1 && y != 0 && y != img.Height - 1)
                    {

                        yStart = y - 1; yFinish = y + 1;
                        xStart = x - 1; xFinish = x;
                      
                    }
                    //Left limit
                    else if (x == 0 && y != 0 && y != img.Height - 1)
                    {
                        yStart = y - 1; yFinish = y + 1;
                        xStart = x; xFinish = x + 1;
                        
                    }
                    else
                    {
                        //Corner Upper left
                        if (y == 0 && x == 0)
                        {
                            yStart = y; yFinish = y + 1;
                            xStart = x; xFinish = x + 1;
                          
                        }
                        //Corner Upper Right
                        if (y == 0 && x == img.Width - 1)
                        {
                            yStart = y; yFinish = y + 1;
                            xStart = x - 1; xFinish = x;
                            
                        }
                        //Corner Botton left
                        if (y == img.Height - 1 && x == img.Width - 1)
                        {
                            yStart = y - 1; yFinish = y;
                            xStart = x - 1; xFinish = x;
                         
                        }
                        //Corner botton Right
                        if (y == img.Height - 1 && x == 0)
                        {
                            yStart = y - 1; yFinish = y;
                            xStart = x; xFinish = x + 1;
                        
                        }
                    }
                    for (int jj = yStart; jj <= yFinish; jj++)
                    {
                        for (int ii = xStart; ii <= xFinish; ii++)
                        {
                            Vec3b info = indexer[jj, ii];
                            sigma = info.Item0 + sigma;
                            sigma1 = info.Item1 + sigma1;
                            sigma2 = info.Item2 + sigma2;

                            if (maxPixelValue < info.Item0)
                            { maxPixelValue = info.Item0; }
                            if (maxPixelValue1 < info.Item1)
                            { maxPixelValue1 = info.Item1; }
                            if (maxPixelValue2 < info.Item2)
                            { maxPixelValue2 = info.Item2; }
                            if (minPixelValue > info.Item0)
                            { minPixelValue = info.Item0; }
                            if (minPixelValue1 > info.Item1)
                            { minPixelValue1 = info.Item1; }
                            if (minPixelValue2 > info.Item2)
                            { minPixelValue2 = info.Item2; }
                        }
                    }
                    sigma = sigma - color.Item0;
                    sigma1 = sigma1 - color.Item1;
                    sigma2 = sigma2 - color.Item2;

                    if (sigma == minPixelValue)
                    {
                        color2.Item0 = Convert.ToByte(minPixelValue2);
                        sigma = 0;
                    }
                    else
                    {
                        sigma = 0;
                    }
                    if (sigma1 == minPixelValue1)
                    {
                        color2.Item1 = Convert.ToByte(minPixelValue1);
                        sigma1 = 0;
                    }
                    else
                    {
                        sigma1 = 0;
                    }
                    if (sigma2 == minPixelValue2)
                    {
                        color2.Item2 = Convert.ToByte(minPixelValue1);
                        sigma2 = 0;
                    }
                    else
                    {
                        sigma2 = 0;
                    }

                    indexer2[y, x] = color2;
                }
            }
            return pepperRemovedImage;
        }

        /// <summary>
        /// Returns a removed noise image.
        /// [2.20]
        /// </summary>
        public Mat RemoveNoise(Mat img)
        {//Declare the return Mat and the indexers for the Mats.
            Mat noiseRemovedImage = new Mat(img.Height, img.Width, img.Type());
            var mat1 = new Mat<Vec3b>(img);
            var indexer = mat1.GetIndexer();
            var mat2 = new Mat<Vec3b>(noiseRemovedImage);
            var indexer2 = mat2.GetIndexer();
            int sigma = 0, sigma1 = 0, sigma2 = 0; ;
            int xStart = 0, yStart = 0, xFinish = 0, yFinish = 0;
            int matrixSize = 0;
            int maxPixelValue = 0, maxPixelValue1 = 0, maxPixelValue2 = 0;
            int minPixelValue = 0, minPixelValue1 = 0, minPixelValue2 = 0;

            //The path on Mat object to remove noise
            for (int y = 0; y < img.Height; y++)
            {
                for (int x = 0; x < img.Width; x++)
                {

                    Vec3b color = indexer[y, x];
                    Vec3b color2 = color;
                    minPixelValue = color.Item0;
                    minPixelValue1 = color.Item1;
                    minPixelValue2 = color.Item2;
                    maxPixelValue = color.Item0;
                    maxPixelValue1 = color.Item1;
                    maxPixelValue2 = color.Item2;
                    //Limits
                    //Center
                    if (x != 0 && x != img.Width - 1 && y != 0 && y != img.Height - 1)
                    {
                        yStart = y - 1; yFinish = y + 1;
                        xStart = x - 1; xFinish = x + 1;
                        matrixSize = 9;
                    }
                    //Upper limit
                    if (y == 0 && x != 0 && x != img.Width - 1)
                    {
                        yStart = y; yFinish = y + 1;
                        xStart = x - 1; xFinish = x + 1;
                        matrixSize = 6;
                    }
                    //Bottom limit
                    else if (y == img.Height - 1 && x != 0 && x != img.Width - 1)
                    {
                        yStart = y - 1; yFinish = y;
                        xStart = x - 1; xFinish = x + 1;
                        matrixSize = 6;
                    }
                    //Right limit
                    else if (x == img.Width - 1 && y != 0 && y != img.Height - 1)
                    {

                        yStart = y - 1; yFinish = y + 1;
                        xStart = x - 1; xFinish = x;
                        matrixSize = 6;
                    }
                    //Left limit
                    else if (x == 0 && y != 0 && y != img.Height - 1)
                    {
                        yStart = y - 1; yFinish = y + 1;
                        xStart = x; xFinish = x + 1;
                        matrixSize = 6;
                    }
                    else
                    {
                        //Corner Upper left
                        if (y == 0 && x == 0)
                        {
                            yStart = y; yFinish = y + 1;
                            xStart = x; xFinish = x + 1;
                            matrixSize = 4;
                        }
                        //Corner Upper Right
                        if (y == 0 && x == img.Width - 1)
                        {
                            yStart = y; yFinish = y + 1;
                            xStart = x - 1; xFinish = x;
                            matrixSize = 4; ;
                        }
                        //Corner Botton left
                        if (y == img.Height - 1 && x == img.Width - 1)
                        {
                            yStart = y - 1; yFinish = y;
                            xStart = x - 1; xFinish = x;
                            matrixSize = 4;
                        }
                        //Corner botton Right
                        if (y == img.Height - 1 && x == 0)
                        {
                            yStart = y - 1; yFinish = y;
                            xStart = x; xFinish = x + 1;
                            matrixSize = 4;
                        }
                    }
                    for (int jj = yStart; jj <= yFinish; jj++)
                    {
                        for (int ii = xStart; ii <= xFinish; ii++)
                        {
                            Vec3b info = indexer[jj, ii];
                            sigma = info.Item0 + sigma;
                            sigma1 = info.Item1 + sigma1;
                            sigma2 = info.Item2 + sigma2;

                            if (maxPixelValue < info.Item0)
                            { maxPixelValue = info.Item0; }
                            if (maxPixelValue1 < info.Item1)
                            { maxPixelValue1 = info.Item1; }
                            if (maxPixelValue2 < info.Item2)
                            { maxPixelValue2 = info.Item2; }
                            if (minPixelValue > info.Item0)
                            { minPixelValue = info.Item0; }
                            if (minPixelValue1 > info.Item1)
                            { minPixelValue1 = info.Item1; }
                            if (minPixelValue2 > info.Item2)
                            { minPixelValue2 = info.Item2; }
                        }
                    }
                    sigma = sigma - color.Item0;
                    sigma1 = sigma1 - color.Item1;
                    sigma2 = sigma2 - color.Item2;

                    if (sigma == minPixelValue)
                    {
                        color2.Item0 = Convert.ToByte(minPixelValue2);
                        sigma = 0;
                    }
                    else if (sigma ==((matrixSize - 1) * maxPixelValue))
                    { 
                        color2.Item0 = Convert.ToByte(maxPixelValue);
                        sigma = 0;
                    }
                    else
                    {
                        sigma = 0;
                    }
                    if (sigma1 == minPixelValue1)
                    {
                        color2.Item1 = Convert.ToByte(minPixelValue1);
                        sigma1 = 0;
                    }
                    else if (sigma1 == ((matrixSize - 1) * maxPixelValue1))
                    {
                        color2.Item1 = Convert.ToByte(maxPixelValue1);
                        sigma = 0;
                    }
                    else
                    {
                        sigma1 = 0;
                    }
                    if (sigma2 == minPixelValue2)
                    {
                        color2.Item2 = Convert.ToByte(minPixelValue1);
                        sigma2 = 0;
                    }
                    else if (sigma2 == ((matrixSize - 1) * maxPixelValue2))
                    {
                        color2.Item2 = Convert.ToByte(maxPixelValue2);
                        sigma = 0;
                    }
                    else
                    {
                        sigma2 = 0;
                    }

                    indexer2[y, x] = color2;
                }
            }
            return noiseRemovedImage;
        }

        /// <summary>
        /// Returns a removed noise image.
        /// [2.21]
        /// </summary>
        public Mat RemoveNoiseSpurs(Mat img)
        {
            //Declare the return Mat and the indexers for the Mats.
            Mat noiseRemovedImage = new Mat(img.Height, img.Width, img.Type());
            var mat1 = new Mat<Vec3b>(img);
            var indexer = mat1.GetIndexer();
            var mat2 = new Mat<Vec3b>(noiseRemovedImage);
            var indexer2 = mat2.GetIndexer();
            int sigma = 0, sigma1 = 0, sigma2 = 0; ;
            int xStart = 0, yStart = 0, xFinish = 0, yFinish = 0;
            int matrixSize = 0;
            int maxPixelValue = 0, maxPixelValue1 = 0, maxPixelValue2 = 0;
            int minPixelValue = 0, minPixelValue1 = 0, minPixelValue2 = 0;

            //The path on Mat object to remove noise
            for (int y = 0; y < img.Height; y++)
            {
                for (int x = 0; x < img.Width; x++)
                {

                    Vec3b color = indexer[y, x];
                    Vec3b color2 = color;
                    minPixelValue = color.Item0;
                    minPixelValue1 = color.Item1;
                    minPixelValue2 = color.Item2;
                    maxPixelValue = color.Item0;
                    maxPixelValue1 = color.Item1;
                    maxPixelValue2 = color.Item2;
                    //Limits
                    //Center
                    if (x != 0 && x != img.Width - 1 && y != 0 && y != img.Height - 1)
                    {
                        yStart = y - 1; yFinish = y + 1;
                        xStart = x - 1; xFinish = x + 1;
                        matrixSize = 9;
                    }
                    //Upper limit
                    if (y == 0 && x != 0 && x != img.Width - 1)
                    {
                        yStart = y; yFinish = y + 1;
                        xStart = x - 1; xFinish = x + 1;
                        matrixSize = 6;
                    }
                    //Bottom limit
                    else if (y == img.Height - 1 && x != 0 && x != img.Width - 1)
                    {
                        yStart = y - 1; yFinish = y;
                        xStart = x - 1; xFinish = x + 1;
                        matrixSize = 6;
                    }
                    //Right limit
                    else if (x == img.Width - 1 && y != 0 && y != img.Height - 1)
                    {

                        yStart = y - 1; yFinish = y + 1;
                        xStart = x - 1; xFinish = x;
                        matrixSize = 6;
                    }
                    //Left limit
                    else if (x == 0 && y != 0 && y != img.Height - 1)
                    {
                        yStart = y - 1; yFinish = y + 1;
                        xStart = x; xFinish = x + 1;
                        matrixSize = 6;
                    }
                    else
                    {
                        //Corner Upper left
                        if (y == 0 && x == 0)
                        {
                            yStart = y; yFinish = y + 1;
                            xStart = x; xFinish = x + 1;
                            matrixSize = 4;
                        }
                        //Corner Upper Right
                        if (y == 0 && x == img.Width - 1)
                        {
                            yStart = y; yFinish = y + 1;
                            xStart = x - 1; xFinish = x;
                            matrixSize = 4; ;
                        }
                        //Corner Botton left
                        if (y == img.Height - 1 && x == img.Width - 1)
                        {
                            yStart = y - 1; yFinish = y;
                            xStart = x - 1; xFinish = x;
                            matrixSize = 4;
                        }
                        //Corner botton Right
                        if (y == img.Height - 1 && x == 0)
                        {
                            yStart = y - 1; yFinish = y;
                            xStart = x; xFinish = x + 1;
                            matrixSize = 4;
                        }
                    }
                    for (int jj = yStart; jj <= yFinish; jj++)
                    {
                        for (int ii = xStart; ii <= xFinish; ii++)
                        {
                            Vec3b info = indexer[jj, ii];
                            sigma = info.Item0 + sigma;
                            sigma1 = info.Item1 + sigma1;
                            sigma2 = info.Item2 + sigma2;

                            if (maxPixelValue < info.Item0)
                            { maxPixelValue = info.Item0; }
                            if (maxPixelValue1 < info.Item1)
                            { maxPixelValue1 = info.Item1; }
                            if (maxPixelValue2 < info.Item2)
                            { maxPixelValue2 = info.Item2; }
                            if (minPixelValue > info.Item0)
                            { minPixelValue = info.Item0; }
                            if (minPixelValue1 > info.Item1)
                            { minPixelValue1 = info.Item1; }
                            if (minPixelValue2 > info.Item2)
                            { minPixelValue2 = info.Item2; }
                        }
                    }
                    sigma = sigma - color.Item0;
                    sigma1 = sigma1 - color.Item1;
                    sigma2 = sigma2 - color.Item2;

                    if (sigma < (minPixelValue + minPixelValue))
                    {
                        color2.Item0 = Convert.ToByte(minPixelValue2);
                        sigma = 0;
                    }
                    else if (sigma >= ((matrixSize - 2) * maxPixelValue))
                    {
                        color2.Item0 = Convert.ToByte(maxPixelValue);
                        sigma = 0;
                    }
                    else
                    {
                        sigma = 0;
                    }
                    if (sigma1 < (minPixelValue1 + minPixelValue1))
                    {
                        color2.Item1 = Convert.ToByte(minPixelValue1);
                        sigma1 = 0;
                    }
                    else if (sigma1 >=  ((matrixSize - 2) * maxPixelValue1))
                    {
                        color2.Item1 = Convert.ToByte(maxPixelValue1);
                        sigma = 0;
                    }
                    else
                    {
                        sigma1 = 0;
                    }
                    if (sigma2 < (minPixelValue2 + minPixelValue2))
                    {
                        color2.Item2 = Convert.ToByte(minPixelValue1);
                        sigma2 = 0;
                    }
                    else if (sigma2 >= ((matrixSize - 1) * maxPixelValue2))
                    {
                        color2.Item2 = Convert.ToByte(maxPixelValue2);
                        sigma = 0;
                    }
                    else
                    {
                        sigma2 = 0;
                    }

                    indexer2[y, x] = color2;
                }
            }
            return noiseRemovedImage;
        }

        /// <summary>
        /// Returns a basic convolutioned Image
        /// [2.22 – 2.26]
        /// </summary>
        public Mat BasicConvolutionImage(Mat img, Mat convolutionMask)
        {
            Mat basicConvolutionImage = new Mat(img.Height, img.Width, img.Type());
            var mat1 = new Mat<Vec3b>(img);
            var indexer = mat1.GetIndexer();
            var mat2 = new Mat<Vec3b>(basicConvolutionImage);
            var indexer2 = mat2.GetIndexer();
            var mat3 = new Mat<float>(convolutionMask);
            var indexer3 = mat3.GetIndexer();
            float pixelValue = 0;
            float pixelValue1 = 0;
            float pixelValue2 = 0;
            int xStart = 0, yStart = 0, xFinish = 0, yFinish = 0;

            for (int y = 0; y < img.Height; y++)
            {
                for (int x = 0; x < img.Width; x++)
                {

                    Vec3b color = indexer[y, x];
                    Vec3b color2 = color;

                    //Limits
                    //Center
                    if (x != 0 && x != img.Width - 1 && y != 0 && y != img.Height - 1)
                    {
                        yStart = y - 1; yFinish = y + 1;
                        xStart = x - 1; xFinish = x + 1;
                    }
                    //Upper limit
                    if (y == 0 && x != 0 && x != img.Width - 1)
                    {
                        yStart = y; yFinish = y + 1;
                        xStart = x - 1; xFinish = x + 1;
                    }
                    //Bottom limit
                    else if (y == img.Height - 1 && x != 0 && x != img.Width - 1)
                    {
                        yStart = y - 1; yFinish = y;
                        xStart = x - 1; xFinish = x + 1;
                    }
                    //Right limit
                    else if (x == img.Width - 1 && y != 0 && y != img.Height - 1)
                    {

                        yStart = y - 1; yFinish = y + 1;
                        xStart = x - 1; xFinish = x;
                    }
                    //Left limit
                    else if (x == 0 && y != 0 && y != img.Height - 1)
                    {
                        yStart = y - 1; yFinish = y + 1;
                        xStart = x; xFinish = x + 1;
                    }
                    else
                    {
                        //Corner Upper left
                        if (y == 0 && x == 0)
                        {
                            yStart = y; yFinish = y + 1;
                            xStart = x; xFinish = x + 1;
                        }
                        //Corner Upper Right
                        if (y == 0 && x == img.Width - 1)
                        {
                            yStart = y; yFinish = y + 1;
                            xStart = x - 1; xFinish = x;
                        }
                        //Corner Botton left
                        if (y == img.Height - 1 && x == img.Width - 1)
                        {
                            yStart = y - 1; yFinish = y;
                            xStart = x - 1; xFinish = x;
                        }
                        //Corner botton Right
                        if (y == img.Height - 1 && x == 0)
                        {
                            yStart = y - 1; yFinish = y;
                            xStart = x; xFinish = x + 1;
                        }
                    }
                    for (int jj = yStart; jj <= yFinish; jj++)
                    {
                        for (int ii = xStart; ii <= xFinish; ii++)
                        {
                            Vec3b info = indexer[jj, ii];
                            float infoKernel = indexer3[yFinish - jj, xFinish - ii];
                            pixelValue = (pixelValue + (info.Item0 * infoKernel));
                            pixelValue1 = (pixelValue1 + (info.Item1 * infoKernel));
                            pixelValue2 = (pixelValue2 + (info.Item2 * infoKernel));

                        }
                    }

                    color2.Item0 = Convert.ToByte(pixelValue); color2.Item1 = Convert.ToByte(pixelValue1); color2.Item2 = Convert.ToByte(pixelValue2);
                    indexer2[y, x] = color2;
                    pixelValue = 0; pixelValue1 = 0; pixelValue2 = 0;
                }
            }
            return basicConvolutionImage;
        }


        /// <summary>
        /// Returns the results first book problem
        /// Computer Vision Principles Algorithms Applications Learning Chapter 2.
        /// </summary>
        public Mat DiffBinary(Mat img, Mat erosionImg)
        {
            Mat diff = new Mat(img.Height, img.Width, MatType.CV_8U);
            var mat1 = new Mat<Vec3b>(img);
            var indexer = mat1.GetIndexer();
            var mat2 = new Mat<Vec3b>(erosionImg);
            var indexer2 = mat2.GetIndexer();
            var mat3 = new Mat<Vec3b>(diff);
            var indexer3 = mat3.GetIndexer();
            for (int y = 0; y < img.Height; y++)
            {
                for (int x = 0; x < img.Width; x++)
                {
                    Vec3b color = indexer[y, x];
                    Vec3b color2 = indexer2[y, x];
                    Vec3b color3 = indexer3[y, x];
                    color3.Item0 = Convert.ToByte(color.Item0 - color2.Item0);
                    color3.Item1 = Convert.ToByte(color.Item1 - color2.Item1);
                    color3.Item2 = Convert.ToByte(color.Item2 - color2.Item2);
                    indexer3[y, x] = color3;
                }

            }
            return diff;
        }
    }
}