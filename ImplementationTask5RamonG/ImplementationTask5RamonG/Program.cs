﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenCvSharp;
using TestImageOperators;
using ClassTask5RamonGiron;

namespace ImplementationTask5RamonG
{
    class Program
    {
        static void Main(string[] args)
        {
            Mat img = Cv2.ImRead(@"..\..\..\Image\stonhenge.png", ImreadModes.Color);
           //Cv2.Threshold(img.Clone().CvtColor(ColorConversionCodes.BGR2GRAY), img, 100, 255, ThresholdTypes.Binary);

            ImageOperatorsclass imgopob = new ImageOperatorsclass();
            TestFunctions tester = new TestFunctions(imgopob);
            Cv2.ImShow("original", img);
            Cv2.WaitKey();

            TestResults results = tester.VisualizeBinary_SameProperties_EqualThresholdedImagesInverted(img);


            Cv2.ImShow("Opencv Result", results.CvMat);
            Cv2.ImShow("Result", results.OwnMat);
            if (results.EqualImages)
            {
                Console.WriteLine("There are the same");
            }
            Cv2.WaitKey();
        }
    }
}
